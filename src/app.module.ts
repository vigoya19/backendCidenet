import { EmployeeModule } from './employee/employee.module';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';

import { TypeOrmModule } from '@nestjs/typeorm';
import { Employee } from './employee/employee.entity';

@Module({
  imports: [
    EmployeeModule,
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: ':memory:', //Indica que es una base de datos en memoria y se reinicia en cada ejecución
      entities: [Employee],
      // entities: [Employee], // Se definen las entidades va administrar (y las cuales creara como tablas al momento de inicar la app)
      synchronize: true,
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
