import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { Logger } from 'src/resources/utils/logger';
import { NOT_FIX_MESSAGE } from '../../../resources/constants/constant-label';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const time = new Date().toLocaleString('en-GB', { timeZone: 'UTC' });
    const logger = new Logger(exception);
    logger.printError();
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();
    const status = exception.getStatus();
    response.status(status).json(
      JSON.parse(
        JSON.stringify({
          statusCode: status,
          type: 'httpException',
          timestamp: time,
          path: request.url,
          message: exception?.message,
          fix:
            (exception?.getResponse() as any)?.message !== exception?.message
              ? (exception?.getResponse() as any)?.message
              : NOT_FIX_MESSAGE,
        }),
      ),
    );
  }
}
