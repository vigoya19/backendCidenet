import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpStatus,
} from '@nestjs/common';
import { QueryFailedError } from 'typeorm';
import { Request, Response } from 'express';
import { InsertValuesMissingError } from 'typeorm/error/InsertValuesMissingError';
import { Logger } from '../../../resources/utils/logger';

@Catch(QueryFailedError, InsertValuesMissingError)
export class OrmExceptionFilter implements ExceptionFilter {
  catch(
    exception: QueryFailedError | InsertValuesMissingError,
    host: ArgumentsHost,
  ) {
    const logger = new Logger(exception);
    logger.printError();
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();
    response.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
      statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
      type: exception.name,
      timestamp: new Date().toISOString(),
      url: request.url,
    });
  }
}
