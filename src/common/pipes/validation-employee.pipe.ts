import { Injectable, PipeTransform, BadRequestException } from '@nestjs/common';
import { ValidationTypes } from 'class-validator';

import { CreateEmployeeDto } from '../../employee/dtos/create.dto';

@Injectable()
export class ValidationEmployee implements PipeTransform {
  transform(value: CreateEmployeeDto): CreateEmployeeDto {
    // this.validateAccents(value);
    return value;
  }

  validateAccents(value) {
    const regAccents = /^[A-Z]+(\s*[A-Z]*)*[A-Z]+$/;
    if (regAccents.test(value.primerApellido))
      throw new BadRequestException(
        'No se permiten acentos y debe ser en MAYUSCULA',
      );
  }
}
