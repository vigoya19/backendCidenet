import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
  ValidationOptions,
  registerDecorator,
} from 'class-validator';

@ValidatorConstraint({ name: 'customText', async: false })
export class CustomTextAccents implements ValidatorConstraintInterface {
  validate(text: string, args: ValidationArguments) {
    const regAccents = /^[A-Z]+(\s*[A-Z]*)*[A-Z]+$/;
    return regAccents.test(text);
  }

  defaultMessage(args: ValidationArguments) {
    // here you can provide default error message if validation failed
    return 'Text ($value) is too short or too long!';
  }
}
