import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
  ValidationOptions,
  registerDecorator,
} from 'class-validator';

@ValidatorConstraint({ name: 'customText', async: false })
export class customTextIdentificationsNumbers
  implements ValidatorConstraintInterface
{
  validate(text: string, args: ValidationArguments) {
    const regAccents = /^([a-zA-Z0-9_-]){1,20}$/;
    return regAccents.test(text);
  }

  defaultMessage(args: ValidationArguments) {
    // here yu can provide default error message if validation failed
    return 'Text ($value) is too short or too long!';
  }
}
