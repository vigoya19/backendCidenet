import { Type } from 'class-transformer';
import {
  IsString,
  IsNotEmpty,
  IsEmail,
  IsOptional,
  IsDefined,
  MinLength,
  IsBase64,
  IsEnum,
  IsNumber,
  Length,
  Min,
  Max,
  IsDate,
  // IsNotEmpty as _IsNotEmpty,
  Validate,
  ValidationOptions,
  MaxLength,
  IsDateString,
} from 'class-validator';
import { customTextIdentificationsNumbers } from 'src/common/validations/customTextIdentificationsNumbers';
import { CustomTextAccents } from '../../common/validations/customTextAccents';
import {
  NOT_FIX_MESSAGE,
  ACCENTS_TEXT,
} from '../../resources/constants/constant-label';
export class CreateEmployeeDto {
  @IsNumber()
  readonly id: number;

  @IsString()
  @Validate(CustomTextAccents, { message: ACCENTS_TEXT })
  @MaxLength(20, { message: NOT_FIX_MESSAGE })
  readonly primerApellido: string;

  @IsString()
  @MaxLength(20, { message: NOT_FIX_MESSAGE })
  readonly segundoApellido: string;

  @IsString()
  @MaxLength(20, { message: NOT_FIX_MESSAGE })
  readonly primerNombre: string;

  @IsString()
  @MaxLength(50, { message: NOT_FIX_MESSAGE })
  readonly otrosNombres: string;

  @IsEmail()
  @IsOptional()
  readonly correo: string;

  @IsDate()
  @IsOptional()
  @Type(() => Date)
  readonly fechaIngreso: Date;

  @IsString()
  readonly area: string;

  @IsString()
  @IsOptional()
  readonly estado: string;

  @IsDate()
  @Type(() => Date)
  readonly fechaRegistro: Date;

  @IsString()
  @Validate(customTextIdentificationsNumbers, {
    message: NOT_FIX_MESSAGE,
  })
  @MaxLength(20, { message: NOT_FIX_MESSAGE })
  readonly numeroIdentificacion: string;

  @IsString()
  readonly paisEmpleo: string;

  @IsString()
  readonly tipoIdentificacion: string;
}
