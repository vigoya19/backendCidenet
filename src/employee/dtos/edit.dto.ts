import { Type } from 'class-transformer';
import {
  IsString,
  IsNotEmpty,
  IsEmail,
  IsOptional,
  IsDefined,
  MinLength,
  IsBase64,
  IsEnum,
  IsNumber,
  Length,
  Min,
  Max,
  IsDate,
  Validate,
  ValidationOptions,
  MaxLength,
  IsDateString,
} from 'class-validator';
import { customTextIdentificationsNumbers } from 'src/common/validations/customTextIdentificationsNumbers';
import { CustomTextAccents } from '../../common/validations/customTextAccents';
import {
  NOT_FIX_MESSAGE,
  ACCENTS_TEXT,
} from '../../resources/constants/constant-label';
export class EditEmployeeDto {
  @IsNumber()
  @IsOptional()
  readonly id: number;

  @IsString()
  @IsOptional()
  @Validate(CustomTextAccents, { message: ACCENTS_TEXT })
  @MaxLength(20, { message: NOT_FIX_MESSAGE })
  readonly primerApellido: string;

  @IsString()
  @IsOptional()
  @MaxLength(20, { message: NOT_FIX_MESSAGE })
  readonly segundoApellido: string;

  @IsString()
  @IsOptional()
  @MaxLength(20, { message: NOT_FIX_MESSAGE })
  readonly primerNombre: string;

  @IsString()
  @IsOptional()
  @MaxLength(50, { message: NOT_FIX_MESSAGE })
  readonly otrosNombres: string;

  @IsEmail()
  @IsOptional()
  @IsOptional()
  readonly correo: string;

  @IsDate()
  @IsOptional()
  @Type(() => Date)
  readonly fechaIngreso: Date;

  @IsString()
  @IsOptional()
  readonly area: string;

  @IsString()
  @IsOptional()
  readonly estado: string;

  @IsDate()
  @IsOptional()
  @Type(() => Date)
  readonly fechaRegistro: Date;

  @IsString()
  @IsOptional()
  @Validate(customTextIdentificationsNumbers, {
    message: NOT_FIX_MESSAGE,
  })
  @MaxLength(20, { message: NOT_FIX_MESSAGE })
  readonly numeroIdentificacion: string;

  @IsString()
  @IsOptional()
  readonly paisEmpleo: string;

  @IsString()
  @IsOptional()
  readonly tipoIdentificacion: string;
}
