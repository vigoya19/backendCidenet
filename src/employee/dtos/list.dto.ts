import { Expose, Type } from 'class-transformer';
import {
  IsString,
  IsNotEmpty,
  IsEmail,
  IsOptional,
  IsDefined,
  MinLength,
  IsBase64,
  IsEnum,
  IsNumber,
  Length,
  Min,
  Max,
  IsDate,
  // IsNotEmpty as _IsNotEmpty,
  Validate,
  ValidationOptions,
  MaxLength,
  IsDateString,
} from 'class-validator';
import { customTextIdentificationsNumbers } from '../../common/validations/customTextIdentificationsNumbers';
import { CustomTextAccents } from '../../common/validations/customTextAccents';
import {
  NOT_FIX_MESSAGE,
  ACCENTS_TEXT,
} from '../../resources/constants/constant-label';
export class ListEmployeeDto {
  @IsNumber()
  @Expose()
  readonly id: number;

  @IsString()
  @Expose()
  readonly primerApellido: string;

  @IsString()
  @Expose()
  readonly segundoApellido: string;

  @IsString()
  @Expose()
  @MaxLength(20, { message: NOT_FIX_MESSAGE })
  readonly primerNombre: string;

  @IsString()
  @Expose()
  @MaxLength(50, { message: NOT_FIX_MESSAGE })
  readonly otrosNombres: string;

  @IsEmail()
  @Expose()
  @IsOptional()
  readonly correo: string;

  @IsDateString()
  @Expose()
  @Type(() => Date)
  readonly fechaIngreso: Date;

  @IsString()
  @Expose()
  readonly area: string;

  @IsString()
  @Expose()
  @IsOptional()
  readonly estado: string;

  @IsDateString()
  @Expose()
  @Type(() => Date)
  readonly fechaRegistro: Date;

  @IsString()
  @Expose()
  @Validate(customTextIdentificationsNumbers, {
    message: NOT_FIX_MESSAGE,
  })
  @MaxLength(20, { message: NOT_FIX_MESSAGE })
  readonly numeroIdentificacion: string;

  @IsString()
  @Expose()
  readonly paisEmpleo: string;

  @IsString()
  @Expose()
  readonly tipoIdentificacion: string;
}
