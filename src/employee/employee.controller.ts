import { Delete, Put } from '@nestjs/common';
import {
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  UsePipes,
} from '@nestjs/common';

import { ValidationEmployee } from 'src/common/pipes/validation-employee.pipe';
import { CreateEmployeeDto } from './dtos/create.dto';
import { Employee } from './employee.entity';
import { EmployeeService } from './employee.service';

@Controller('employee')
export class EmployeeController {
  constructor(private employeeService: EmployeeService) {}

  @UsePipes(ValidationEmployee)
  @Post()
  guardarEmployee(@Body() employee: Employee): Promise<CreateEmployeeDto> {
    return this.employeeService.create(employee);
  }

  @Get(':id')
  listarPorId(@Param('id', ParseIntPipe) id: number) {
    return this.employeeService.getById(id);
  }

  @Put(':id')
  editarEmployee(
    @Param('id', ParseIntPipe) id: number,
    @Body() employee: Employee,
  ) {
    return this.employeeService.edit(id, employee);
  }

  @Get()
  listar() {
    return this.employeeService.list();
  }

  @Delete(':id')
  eliminarEmployee(@Param() id: Employee) {
    return this.employeeService.remove(id);
  }
}
