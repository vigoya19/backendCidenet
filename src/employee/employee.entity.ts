import { Estado } from '../resources/enums/estado';
import { TipoIdentificacion } from 'src/resources/enums/tipoIdentificacion';
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';

import { Pais } from '../resources/enums/pais';
import { Area } from 'src/resources/enums/area';

@Entity()
export class Employee {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 20 })
  primerApellido: string;

  @Column({ length: 20 })
  segundoApellido: string;

  @Column({ length: 20 })
  primerNombre: string;

  @Column({ length: 50 })
  otrosNombres: string;

  @Column({ length: 20 })
  numeroIdentificacion: string;

  @Column({
    enum: Pais,
  })
  paisEmpleo: string;

  @Column({
    enum: TipoIdentificacion,
  })
  tipoIdentificacion: string;

  @Column()
  correo: string;

  @Column()
  fechaIngreso: Date;

  @Column({ enum: Area })
  area: string;

  @Column({ enum: Estado, default: Estado.ACTIVO })
  estado: string;

  @Column({ default: Date() })
  fechaRegistro: Date;
}
