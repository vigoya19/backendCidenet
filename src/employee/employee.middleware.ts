import {
  BadRequestException,
  Injectable,
  NestMiddleware,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { EmployeeService } from './employee.service';
import { Pais, Dominio } from '../resources/enums/pais';

@Injectable()
export class EmployeeMiddleware implements NestMiddleware {
  constructor(private employeeService: EmployeeService) {}

  async use(req: Request, res: Response, next: Function) {
    let { body } = req;
    const fechaIngreso = new Date();
    body.fechaIngreso = fechaIngreso;
    const correo = await this.createEmail(body);
    await this.validateIdentificationNumber(body);
    this.validateDate(body);
    body = Object.assign(body, { correo });

    next();
  }

  async createEmail({ primerNombre, primerApellido, paisEmpleo }) {
    const { COLOMBIA } = Pais;
    const { DOMINIO_COLOMBIA, DOMINIO_ESTADOS_UNIDOS } = Dominio;
    const dominio =
      paisEmpleo === COLOMBIA ? DOMINIO_COLOMBIA : DOMINIO_ESTADOS_UNIDOS;

    const correo = `${primerNombre}.${primerApellido}@${dominio}`;
    const idEmployee = await this.getIdEmployee(correo);

    const emailSecuence = `${primerNombre}.${primerApellido}${
      idEmployee || ''
    }@${dominio}`;

    return emailSecuence;
  }

  async getIdEmployee(correo: string): Promise<string> {
    const employee = await this.employeeService.getEmployeeByEmail({
      correo,
    });
    if (employee.length) {
      const theLastEmployee = employee.pop();
      return `.${theLastEmployee.id + 1}`;
    }
  }

  async validateIdentificationNumber({ numeroIdentificacion }) {
    const employee =
      await this.employeeService.getEmployeeByIdentificationNumber({
        numeroIdentificacion,
      });
    if (employee.length) {
      throw new BadRequestException(
        `El numero de identificacion ${numeroIdentificacion} Ya se encuentra repetido`,
      );
    }
  }

  validateDate({ fechaIngreso }) {
    const currentDate = new Date();
    const inputDate = new Date(fechaIngreso);
    const minDate = new Date(new Date().setDate(new Date().getDate() - 30));
    console.log(`
     
     fecha actual ${currentDate}, 
     fecha ingreso ${inputDate},
     fecha minima permitida ${minDate}`);

    if (inputDate < minDate)
      throw new BadRequestException(
        `${inputDate.toLocaleDateString(
          'es-CO',
        )} No puede ser menor a mas de 1 mes `,
      );

    if (inputDate > new Date())
      throw new BadRequestException(
        `${inputDate.toLocaleDateString(
          'es-CO',
        )} No puede ser mayor a la fecha actual `,
      );
  }
}
