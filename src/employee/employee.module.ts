import { EmployeeService } from './employee.service';
import { EmployeeController } from './employee.controller';

import { MiddlewareConsumer, Module, RequestMethod } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Employee } from './employee.entity';
import { EmployeeMiddleware } from './employee.middleware';

@Module({
  imports: [TypeOrmModule.forFeature([Employee])],
  controllers: [EmployeeController],
  providers: [EmployeeService],
})
export class EmployeeModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(EmployeeMiddleware)
      .forRoutes({ path: 'employee', method: RequestMethod.POST });
  }
}
