/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { Employee } from './employee.entity';
import { plainToClass } from 'class-transformer';
import { ListEmployeeDto } from './dtos/list.dto';
import { CreateEmployeeDto } from './dtos/create.dto';
import { EditEmployeeDto } from './dtos/edit.dto';
import { identity } from 'rxjs';

@Injectable()
export class EmployeeService {
  constructor(
    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>,
  ) {}

  async create(employee: Partial<CreateEmployeeDto>): Promise<Employee> {
    const employeeSaved: Employee = await this.employeeRepository.save(
      employee,
    );
    return plainToClass(CreateEmployeeDto, employeeSaved);
  }

  async edit(
    id: number,
    employee: Partial<EditEmployeeDto>,
  ): Promise<Employee> {
    const employeeEdited: UpdateResult = await this.employeeRepository.update(
      id,
      employee,
    );
    return plainToClass(EditEmployeeDto, employeeEdited);
  }

  async remove(id): Promise<DeleteResult> {
    const employeeDeleted = await this.employeeRepository.delete(id);
    return employeeDeleted;
  }

  async list(): Promise<ListEmployeeDto[]> {
    const pagoList: Employee[] = await this.employeeRepository.find();
    return plainToClass(ListEmployeeDto, pagoList);
  }

  async getById(id: number): Promise<ListEmployeeDto[]> {
    const pagoList: Employee[] = await this.employeeRepository.find({ id });
    return plainToClass(ListEmployeeDto, pagoList);
  }

  async getEmployeeByEmail(correo): Promise<Employee[]> {
    return this.employeeRepository.find(correo);
  }

  async getEmployeeByIdentificationNumber(
    numeroIdentificacion,
  ): Promise<Employee[]> {
    return this.employeeRepository.find(numeroIdentificacion);
  }
}
