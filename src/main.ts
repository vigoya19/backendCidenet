import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

import { HttpExceptionFilter } from './common/filters/http-exception/http-exception.filter';
import { ErrorExceptionFilter } from './common/filters/error-exception/error-exception.filter';
import { OrmExceptionFilter } from './common/filters/orm-exception/orm-exception.filter';
import { ResponseGenericInterceptor } from './common/interceptors/response-generic.interceptor';
import { ValidationPipe } from '@nestjs/common';
import * as helmet from 'helmet';
import * as bodyParser from 'body-parser';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  app.use(helmet());

  app.useGlobalFilters(
    new ErrorExceptionFilter(),
    new HttpExceptionFilter(),
    new OrmExceptionFilter(),
  );

  // app.useGlobalPipes(
  //   new ValidationPipe({
  //     transform: true,
  //     whitelist: true,
  //     forbidNonWhitelisted: true,
  //   }),
  // );

  app.useGlobalInterceptors(new ResponseGenericInterceptor());

  await app.listen(3000);
}
bootstrap();
