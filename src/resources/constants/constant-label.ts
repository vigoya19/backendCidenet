export const EMAIL_EXISTS_MESSAGE = 'You are already registered, enter now';
export const NOT_FIX_MESSAGE = 'Not fix for show';
export const UNAUTHORIZED_MESSAGE =
  "You don't have enough permissions !, to do this";
export const INTERNAL_SERVER_ERROR = 'Internal Server Error';
export const MAX_LENGTH_TEXT =
  'La longitud maxima permitida es de 20 caracteres';
export const ACCENTS_TEXT =
  'No deberia contener acentos y todo debe estar en mayuscula';
