export enum Area {
  ADMINISTRACION = 'Administracion',
  FINANCIERA = 'Financiera',
  COMPRAS = 'Compras',
  INFRAESTRUCTURA = 'Infraestructura',
  OPERACION = 'Operacion',
  TALENTO_HUMANO = 'Talento Humano',
  SERVICIOS_VARIOS = 'Servicios Varios',
}
