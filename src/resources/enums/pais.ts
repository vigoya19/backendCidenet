export enum Pais {
  COLOMBIA = 'Colombia',
  ESTADOS_UNIDOS = 'Estados Unidos',
}

export enum Dominio {
  DOMINIO_COLOMBIA = 'cidenet.com.co',
  DOMINIO_ESTADOS_UNIDOS = 'cidenet.com.us',
}
