export enum TipoIdentificacion {
  CEDULA_CIUDADANIA = 'Cedula de Ciudadania',
  CEDULA_EXTRANJERIA = 'Cedula de Extranjeria',
  PASAPORTE = 'Pasaporte',
  PERMISO_ESPECIAL = 'Permiso Especial',
}
