import { HttpException } from '@nestjs/common';
import { OrmExceptionFilter } from 'src/common/filters/orm-exception/orm-exception.filter';
import { InsertValuesMissingError, QueryFailedError } from 'typeorm';
import * as colors from 'colors';

export class Logger {
  private _response: any;

  constructor(response?: Error | QueryFailedError | OrmExceptionFilter) {
    this._response = response;
  }

  get response(): any {
    return this._response;
  }

  set response(response: any) {
    this._response = response;
  }

  public printError() {
    if (this._response instanceof HttpException) {
      console.log(`
    ********** HttpException Description Error ***************
    *                                                        *
    * ${colors.red('Message')}    ==========>     ${
        this.response.message.red
      }                                                                           
    *________________________________________________________*
    *                                                        *
    * ${colors.blue('Name')}      ===========>     ${
        this.response.name.blue
      }                                                                                                       
    *________________________________________________________*                                                                    
    *                                                        *
    * ${colors.yellow('Status')}      ===========>     ${
        JSON.stringify(this.response.getStatus()).yellow || ''
      }                                                                                                
    *________________________________________________________*                                                                    
    *                                                        *
    * ${colors.green('Response')}     =================>    
      ${colors.green(JSON.stringify(this.response.getResponse()))}              
    *                                                        *
    * ${'Ocurred at: '} ${colors.grey(
        JSON.stringify(new Date().toISOString()),
      )} 
    *                                                        *
    *  ${colors.yellow(this.response.stack)}                  *
    `);
    } else if (
      this._response instanceof QueryFailedError ||
      this._response instanceof InsertValuesMissingError
    ) {
      console.log(`
      ******************* ORM Exception Description Error ******************
      *                                                        *
      * ${'Query'.red}    ==========>     ${
        this.response.query.red
      }                                                                           
      *________________________________________________________*
      *                                                        *
      * ${'Parameters'.blue}      ===========>     ${
        JSON.stringify(this.response.parameters).blue
      }                                                                                                       
      *________________________________________________________*                                                                    
      *                                                        *
      * ${'Status'.green}      ===========>     ${JSON.stringify(500).green}
      }                                                                                                
      *________________________________________________________*                                                                    
      *                                                        *            
      *                                                        *
      * ${'Ocurred at: '} ${JSON.stringify(new Date().toISOString()).grey} 
      *   
      * ${'Stack'.yellow} ==============> ${this.response.stack.yellow}
      *                                                     
      `);
    } else if (this._response instanceof Error) {
      console.log(`
      *******************   Description Error ******************
      *                                                        *
      * ${'Message'.red}    ==========>     ${
        this.response.message.red
      }                                                                           
      *________________________________________________________*
      *                                                        *
      * ${'Name'.blue}      ===========>     ${
        this.response.name.blue
      }                                                                                                                    
      *________________________________________________________*
      *
      * ${'Status'.green} =============>  ${JSON.stringify(500).green}
      *
      * ${'Ocurred at: '} ${JSON.stringify(new Date().toISOString()).grey} 
      *  ______________________________________________________*  
      *                                                     
      *  ${'stack'.yellow}  ==========>    ${this.response.stack.yellow}       
      * 
      *                     
      `);
    }
  }
}
